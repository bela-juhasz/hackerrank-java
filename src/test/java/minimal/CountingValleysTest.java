package minimal;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CountingValleysTest {

    // Complete the countingValleys function below.
    static int countingValleys(final int numberOfSteps, final String steps) {
        int level = 0;
        boolean weCameFromAValley = false;
        boolean weJustStarted = true;
        int numberOfValleys = 0;

        for (int i=0; i < numberOfSteps; i++) {
            final char step = steps.charAt(i);

            switch (step) {
                case 'U':
                    level++;
                    break;
                case 'D':
                    level--;
                    break;
                default:
            }

            if (level < 0) {
                weCameFromAValley = true;
            }

            if (level == 0 && weJustStarted == false && weCameFromAValley) {
                numberOfValleys++;
                weCameFromAValley = false;
            }

            weJustStarted = false;
        }

        return numberOfValleys;
    }

    @Test
    public void test1() {
        final int result = countingValleys(8, "UDDDUDUU");
        assertThat(result).isEqualTo(1);
    }

}
