package minimal;

import org.junit.Test;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class FindTheNearestCloneInDirectedGraphTest {

    private static final int NOT_FOUND = -1;

    static int findShortest(int graphNodes, int[] graphFrom, int[] graphTo, long[] ids, int expectedColor) {
        final Map<Integer, Node> nodes = buildGraph(graphNodes, graphFrom, graphTo, ids);
        int minLevel = Integer.MAX_VALUE;

        for (final Node node : nodes.values()) {
            if (node.color != expectedColor) {
                continue;
            }

            int currentLevel = levelReturningBreadthFirstSearch(node, expectedColor);

            if (currentLevel != NOT_FOUND && currentLevel < minLevel) {
                minLevel = currentLevel;
            }
        }

        if (minLevel == Integer.MAX_VALUE) { // there was no update
            return NOT_FOUND;
        } else {
            return minLevel;
        }
    }

    private static void iterativeDepthFirstSearch(final Node startingNode) {
        iterativeDepthFirstSearch(startingNode, null);
    }

    private static void iterativeDepthFirstSearch(final Node startingNode, final Consumer<Node> fun) {
        final Deque<Node> stack = new ArrayDeque<>();

        stack.addFirst(startingNode);

        while (stack.size() != 0) {
            final Node node = stack.removeFirst();

            if (fun != null) {
                fun.accept(node);
            }

            if (!node.discovered) {
                node.discovered = true;
                stack.addAll(node.children);
            }
        }
    }

    private static void originalBreadthFirstSearch(final Node startingNode, final Consumer<Node> fun) {
        final Deque<Node> openQueue = new ArrayDeque<>();
        final Set<Node> closedSet = new HashSet<>();

        openQueue.addLast(startingNode);

        while (openQueue.size() != 0) {

            final Node subTreeRoot = openQueue.removeFirst();

            if (fun != null) {
                fun.accept(subTreeRoot);
            }

            for (final Node child : subTreeRoot.children) {
                if (closedSet.contains(child)) {
                    continue;
                }

                if (!openQueue.contains(child)) {
                    openQueue.addLast(child);
                }
            }

            closedSet.add(subTreeRoot);
        }
    }

    private static int levelReturningBreadthFirstSearch(final Node startingNode, final int expectedColor) {
        final Deque<Node> openQueue = new ArrayDeque<>();
        final Set<Node> closedSet = new HashSet<>();

        startingNode.level = 0;
        openQueue.addLast(startingNode);

        while (openQueue.size() != 0) {

            final Node subTreeRoot = openQueue.removeFirst();

            if (subTreeRoot.color == expectedColor && subTreeRoot != startingNode) {
                return subTreeRoot.level;
            }

            for (final Node child : subTreeRoot.children) {

                if (closedSet.contains(child)) {
                    continue;
                }

                if (!openQueue.contains(child)) {
                    child.level = subTreeRoot.level + 1;
                    openQueue.addLast(child);
                }
            }

            closedSet.add(subTreeRoot);
        }

        return NOT_FOUND;
    }

    private static Map<Integer, Node> buildGraph(final int numberOfGraphNodes, final int[] graphFrom, final int[] graphTo, final long[] colors) {
        final Map<Integer, Node> nodes = new HashMap<>();

        for (int i = 0; i < graphFrom.length; i++) {
            int fromNodeId = graphFrom[i];
            int toNodeId = graphTo[i];
            long color = colors[i];

            final Node fromNode = getOrCreateNodeById(fromNodeId, color, nodes);
            final Node toNode = getOrCreateNodeById(toNodeId, colors[toNodeId-1], nodes);

            fromNode.children.add(toNode);
            toNode.parent = fromNode;
        }

        return nodes;
    }

    private static Node getOrCreateNodeById(final int id, final long color, final Map<Integer, Node> nodes) {
        if (nodes.containsKey(id)) {
            return nodes.get(id);
        } else {
            final Node newNode = new Node(id, color);
            nodes.put(id, newNode);
            return newNode;
        }
    }

    public static class Node {
        public int id;
        public long color;
        public Node parent;
        public List<Node> children = new ArrayList<>();
        public boolean discovered;
        public int level;

        public Node(int id, long color) {
            this.id = id;
            this.color = color;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            final Node node = (Node) o;
            return id == node.id;
        }

        @Override
        public int hashCode() {
            return Objects.hash(id);
        }

        @Override
        public String toString() {
            return "Node{" +
                "id=" + id +
                ", color=" + color +
                (discovered ? ", discovered" : ", not discovered") +
                ", level=" + level +
                ", relatives=" + children.stream().map(n -> n.id).collect(Collectors.toList()) +
                '}';
        }
    }

    @Test
    public void test1() {

        // static int collectDistances(int graphNodes, int[] graphFrom, int[] graphTo, long[] ids, int val) {
        assertThat(findShortest(4,
            new int[]{1, 1, 4},
            new int[]{2, 3, 2},
            new long[]{1, 2, 1, 1}, 1))
            .isEqualTo(1);

        assertThat(findShortest(4,
            new int[]{1, 1, 4},
            new int[]{2, 3, 2},
            new long[]{1, 2, 3, 4}, 2))
            .isEqualTo(-1);

        assertThat(findShortest(5,
            new int[]{1, 1, 2, 3},
            new int[]{2, 3, 4, 5},
            new long[]{1, 2, 3, 3, 2}, 2))
            .isEqualTo(3);
    }
}

