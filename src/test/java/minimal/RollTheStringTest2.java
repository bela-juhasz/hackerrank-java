class Result {

    /*
     * Complete the 'rollTheString' function below.
     *
     * The function is expected to return a STRING.
     * The function accepts following parameters:
     *  1. STRING s
     *  2. INTEGER_ARRAY roll
     */

    public static String rollTheString(String s, List<Integer> roll) {

        int len = s.length();
        int[] charCodes = new int[len];

        char[] charArray = s.toCharArray();

        for (int i = 0; i < len; i++) {
            charCodes[i] = (int) charArray[i] - 97;
        }

        long[] rollPositionSum = new long[len];

        for (int i = 0; i < roll.size(); i++) {
            final Integer rol = roll.get(i);
            rollPositionSum[rol - 1]++;

            if (rollPositionSum[rol - 1] > 25) {
                rollPositionSum[rol - 1] -= 26;
            }
        }

        long rollSum = 0;

        for (int i = len-1; i >= 0; i--) {
            rollSum += rollPositionSum[i];

            if (rollSum > 25) {
                rollSum -= 26;
            }

            charCodes[i] += rollSum;

            if (charCodes[i] > 25) {
                charCodes[i] = charCodes[i] - 26;
            }
        }

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < len; i++) {
            char c = (char) (charCodes[i] + 97);
            sb.append(c);
        }

        return sb.toString();

    }

}

