package minimal;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Trivial test class. Demonstrates the syntax of JUnit4.
 * Important: Do NOT inherit this class from TestCase() or JUnit3.x is enforced
 *
 * @author Sascha Tayefeh
 */
public class DoSomeActionTest {
    @Test
    public void testIsThisReallyTrue() {
        assertThat(true).isTrue();
    }
}
