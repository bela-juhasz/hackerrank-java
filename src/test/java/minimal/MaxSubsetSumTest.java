package minimal;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MaxSubsetSumTest {

    static int maxSubsetSum(int[] arr) {
        /*
        for (int i = 0; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
        }
*/
        return 0;
    }

    static long arrayManipulation(int n, int[][] queries) {
        long[] array = new long[n];

        for (int q = 0; q < queries.length; q++) {
            int[] query = queries[q];
            int start = query[0] - 1;
            int end = query[1] - 1;
            long value = query[2];

            array[start] += value;

            if (end + 1 < n) {
                array[end + 1] -= value;
            }
        }
        
        return maxValue(array);
    }

    private static long maxValue(final long[] array) {
        long max = Long.MIN_VALUE;
        long tmp = 0;

        for (int i = 0; i < array.length; i++) {
            tmp += array[i];

            if (tmp > max) {
                max = tmp;
            }
        }

        return max;
    }


    private static long findMaxValue(final long[] array) {
        long max = Long.MIN_VALUE;

        for (int i = 0; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
        }

        return max;
    }

    @Test
    public void test1() {
        assertThat(maxSubsetSum(new int[] { 3, 7, 4, 6, 5 })).isEqualTo(13);
        assertThat(maxSubsetSum(new int[] { 2, 1, 5, 8, 4 })).isEqualTo(11);
        assertThat(maxSubsetSum(new int[] { 3, 5, -7, 8, 10 })).isEqualTo(15);
    }

}
