package minimal;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;

import static org.assertj.core.api.Assertions.assertThat;

public class ZombieClustersTest {

    public static int zombieCluster(List<String> zombies) {
        int N = zombies.size();

        int[][] M = new int [N][N];
        for(int i = 0; i<N; i++) {
            for (int j = 0 ; j<N; j++) {
                M[i][j] = Integer.parseInt(zombies.get(i).charAt(j) + "");
            }
        }

        boolean visited[] = new boolean[N];
        boolean visiting[] = new boolean[N];

        int count = 0;
        for (int i = 0; i < N; ++i) {
            if(!visited[i]) {
                visiting[i] = true;
                DFS(M, N, visited, visiting, i);
                visited[i] = true;
                count++;
            }
        }
        return count;
    }

    static void DFS(int M[][], int N, boolean visited[], boolean[] visiting, int s)
    {
        if( !visited[s] ) {
            visiting[s] = true;
            for(int j = s+1; j < N; j++) {
                if(M[s][j] == 1 && !visited[j]) {
                    visiting[j] = true;
                    DFS(M, N, visited, visiting, j);
                    visited[j] = true;
                }
            }
        }
    }

    static String[] braces(String[] values) {
        String[] results = new String[values.length];
        for (int i=0; i < values.length; i++) {
            results[i] = braces2(values[i]);
        }
        return results;
    }

    private static String braces2(String s) {
        Stack<Character> stack = new Stack<>();

        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);

            if (c == '[' || c == '(' || c == '{') {
                stack.push(c);
            } else if (c == ']') {
                if (stack.isEmpty() || stack.pop() != '[') {
                    return "NO";
                }
            } else if (c == ')') {
                if (stack.isEmpty() || stack.pop() != '(') {
                    return "NO";
                }
            } else if (c == '}') {
                if (stack.isEmpty() || stack.pop() != '{') {
                    return "NO";
                }
            }

        }

        return stack.isEmpty() ? "YES" : "NO";
    }

    static int jumpingOnClouds(int[] clouds) {
        int i = 0;
        int jumps = 0;

        while (i < clouds.length - 1) {
            int further = i + 2;
            if (further < clouds.length && clouds[further] != 1) {
                i += 2;
            } else {
                i++;
            }

            jumps++;
        }

        return jumps;
    }

    @Test
    public void test1() {
        final int result = zombieCluster(Arrays.asList("1100", "1110", "0110", "0001"));
        assertThat(result).isEqualTo(2);
    }

}
