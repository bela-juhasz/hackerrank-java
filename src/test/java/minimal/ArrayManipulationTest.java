package minimal;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ArrayManipulationTest {

    static long arrayManipulation(int n, int[][] queries) {
        long[] array = new long[n];

        for (int q = 0; q < queries.length; q++) {
            int[] query = queries[q];
            int start = query[0] - 1;
            int end = query[1] - 1;
            long value = query[2];

            array[start] += value;

            if (end + 1 < n) {
                array[end + 1] -= value;
            }
        }
        
        return maxValue(array);
    }

    private static long maxValue(final long[] array) {
        long max = Long.MIN_VALUE;
        long tmp = 0;

        for (int i = 0; i < array.length; i++) {
            tmp += array[i];

            if (tmp > max) {
                max = tmp;
            }
        }

        return max;
    }


    private static long findMaxValue(final long[] array) {
        long max = Long.MIN_VALUE;

        for (int i = 0; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
        }

        return max;
    }

    @Test
    public void test1() {
        assertThat(arrayManipulation(5, new int[][] {
            { 1, 2, 100 },
            { 2, 5, 100 },
            { 3, 4, 100 }
        })).isEqualTo(200);

        assertThat(arrayManipulation(10, new int[][] {
            { 2, 6, 8 },
            { 3, 5, 7 },
            { 1, 8, 1 },
            { 5, 9, 15 }
        })).isEqualTo(31);
    }

}
