package minimal;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MinimumBribesTest {

    static void minimumBribes(int[] q) {
        final long bribes = calculateMinimumBribes(q);

        if (bribes == -1) {
            System.out.println("Too chaotic");
        } else {
            System.out.println(bribes);
        }
    }

    private static long calculateMinimumBribes(int[] queue) {
        int bribes = 0;

        for (int arrayIndex = 0; arrayIndex < queue.length; arrayIndex++) {
            int person = queue[arrayIndex];
            int originalPosition = person;
            int currentPosition = arrayIndex + 1;

            int diff = originalPosition - currentPosition;

            if (diff == 1 || diff == 2) {
                bribes += diff;
            } else if (diff > 2) {
                return -1;
            } else if (diff < -2) {
                bribes += (diff * -1) - 2;
            }

            /*
            if (currentPosition >= originalPosition) {
                bribes += 0;
            } else if (currentPosition < originalPosition - 2) {
                return -1;
            } else {
                bribes += originalPosition - currentPosition;
            }
            */
        }

        return bribes;
    }

    @Test
    public void test1() {
        assertThat(calculateMinimumBribes(new int[] { 2, 1, 5, 3, 4 })).isEqualTo(3);
        assertThat(calculateMinimumBribes(new int[] { 2, 5, 1, 3, 4 })).isEqualTo(-1);
        assertThat(calculateMinimumBribes(new int[] { 5, 1, 2, 3, 7, 8, 6, 4 })).isEqualTo(-1);
        assertThat(calculateMinimumBribes(new int[] { 1, 2, 3, 4, 5, 6, 7, 8 })).isEqualTo(0);
        assertThat(calculateMinimumBribes(new int[] { 1, 2, 5, 3, 7, 4, 6, 8 })).isEqualTo(4);
        assertThat(calculateMinimumBribes(new int[] { 1, 2, 5, 3, 7, 6, 4, 8 })).isEqualTo(5);
        assertThat(calculateMinimumBribes(new int[] { 1, 2, 5, 3, 7, 8, 6, 4 })).isEqualTo(7);


        // 1  2  3  4  5  6  7  8
        // 1  2  5  3  7  6  4  8
        // 0  0 +2 -1 +2  0 -3  0
        // +4 -4

    }

}
