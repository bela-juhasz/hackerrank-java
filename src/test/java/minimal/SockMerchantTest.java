package minimal;

import org.junit.Test;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

import static org.assertj.core.api.Assertions.assertThat;

public class SockMerchantTest {

    // Complete the countingValleys function below.
    static int sockMerchant(int numberOfSocks, int[] socks) {
        ConcurrentMap<Integer, AtomicInteger> pairs = new ConcurrentHashMap<>();

        for (int i=0; i < numberOfSocks; i++) {
            int sock = socks[i];
            pairs.putIfAbsent(sock, new AtomicInteger(0));
            pairs.get(sock).incrementAndGet();
        }

        int sum = 0;

        for (Map.Entry<Integer, AtomicInteger> entry : pairs.entrySet())
        {
            sum += entry.getValue().get() / 2;
        }

        return sum;
    }

    @Test
    public void test1() {
        final int result = sockMerchant(9, new int[]{10, 20, 20, 10, 10, 30, 50, 10, 20});
        assertThat(result).isEqualTo(3);
    }

}
