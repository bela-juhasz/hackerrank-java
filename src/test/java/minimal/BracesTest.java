package minimal;

import org.junit.Test;

import java.util.Arrays;
import java.util.Stack;

import static org.assertj.core.api.Assertions.assertThat;

public class BracesTest {

    static String[] braces(String[] values) {
        String[] results = new String[values.length];
        for (int i=0; i < values.length; i++) {
            results[i] = braces2(values[i]);
        }
        return results;
    }

    private static String braces2(String s) {
        Stack<Character> stack = new Stack<>();

        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);

            if (c == '[' || c == '(' || c == '{') {
                stack.push(c);
            } else if (c == ']') {
                if (stack.isEmpty() || stack.pop() != '[') {
                    return "NO";
                }
            } else if (c == ')') {
                if (stack.isEmpty() || stack.pop() != '(') {
                    return "NO";
                }
            } else if (c == '}') {
                if (stack.isEmpty() || stack.pop() != '{') {
                    return "NO";
                }
            }

        }

        return stack.isEmpty() ? "YES" : "NO";
    }

    static int jumpingOnClouds(int[] clouds) {
        int i = 0;
        int jumps = 0;

        while (i < clouds.length - 1) {
            int further = i + 2;
            if (further < clouds.length && clouds[further] != 1) {
                i += 2;
            } else {
                i++;
            }

            jumps++;
        }

        return jumps;
    }

    @Test
    public void test1() {
        final String[] result = braces(new String[]{"{}[]()", "{[}]}"});
        assertThat(result).containsExactly("YES", "NO");
    }

}
