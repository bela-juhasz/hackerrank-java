package minimal;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

import static org.assertj.core.api.Assertions.assertThat;

public class CanReachTest {

    static int gcd(int a, int b) {
        return b == 0 ? a : gcd(b, a % b);
    }

    public static String canReach(int x1, int y1, int x2, int y2) {
        if (x1 <= x2 && y1 <= y2 && (gcd(x1, y1) == gcd(x2, y2))) {
            return "Yes";
        } else {
            return "No";
        }

    }


    public static void main2() throws Exception {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for(int t = 0; t < n; t++) {

            int numberOfNodes = sc.nextInt();
            int[] nodes = new int[numberOfNodes];

            for(int t2 = 0; t2 < numberOfNodes; t2++) {
                int b = sc.nextInt();
                nodes[t2] = b;
            }

            System.out.println(canRepresentBST(nodes, numberOfNodes));


        }
    }


    static String canRepresentBST(int pre[], int n) {
        // Create an empty stack
        Stack<Integer> s = new Stack<Integer>();

        // Initialize current root as minimum possible
        // value
        int root = Integer.MIN_VALUE;

        // Traverse given array
        for (int i = 0; i < n; i++) {
            // If we find a node who is on right side
            // and smaller than root, return false
            if (pre[i] < root) {
                return "NO";
            }

            // If pre[i] is in right subtree of stack top,
            // Keep removing items smaller than pre[i]
            // and make the last removed item as new
            // root.
            while (!s.empty() && s.peek() < pre[i]) {
                root = s.peek();
                s.pop();
            }

            // At this point either stack is empty or
            // pre[i] is smaller than root, push pre[i]
            s.push(pre[i]);
        }
        return "YES";
    }



    public static int zombieCluster(List<String> zombies) {
        int N = zombies.size();

        int[][] M = new int [N][N];
        for(int i = 0; i<N; i++) {
            for (int j = 0 ; j<N; j++) {
                M[i][j] = Integer.parseInt(zombies.get(i).charAt(j) + "");
            }
        }

        boolean visited[] = new boolean[N];
        boolean visiting[] = new boolean[N];

        int count = 0;
        for (int i = 0; i < N; ++i) {
            if(!visited[i]) {
                visiting[i] = true;
                DFS(M, N, visited, visiting, i);
                visited[i] = true;
                count++;
            }
        }
        return count;
    }

    static void DFS(int M[][], int N, boolean visited[], boolean[] visiting, int s)
    {
        if( !visited[s] ) {
            visiting[s] = true;
            for(int j = s+1; j < N; j++) {
                if(M[s][j] == 1 && !visited[j]) {
                    visiting[j] = true;
                    DFS(M, N, visited, visiting, j);
                    visited[j] = true;
                }
            }
        }
    }

    static String[] braces(String[] values) {
        String[] results = new String[values.length];
        for (int i=0; i < values.length; i++) {
            results[i] = braces2(values[i]);
        }
        return results;
    }

    private static String braces2(String s) {
        Stack<Character> stack = new Stack<>();

        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);

            if (c == '[' || c == '(' || c == '{') {
                stack.push(c);
            } else if (c == ']') {
                if (stack.isEmpty() || stack.pop() != '[') {
                    return "NO";
                }
            } else if (c == ')') {
                if (stack.isEmpty() || stack.pop() != '(') {
                    return "NO";
                }
            } else if (c == '}') {
                if (stack.isEmpty() || stack.pop() != '{') {
                    return "NO";
                }
            }

        }

        return stack.isEmpty() ? "YES" : "NO";
    }

    static int jumpingOnClouds(int[] clouds) {
        int i = 0;
        int jumps = 0;

        while (i < clouds.length - 1) {
            int further = i + 2;
            if (further < clouds.length && clouds[further] != 1) {
                i += 2;
            } else {
                i++;
            }

            jumps++;
        }

        return jumps;
    }

    @Test
    public void test1() {
        final int result = zombieCluster(Arrays.asList("1100", "1110", "0110", "0001"));
        assertThat(result).isEqualTo(2);
    }

}
