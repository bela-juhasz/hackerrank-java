package minimal;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class JumpingOnCloudsTest {

    static int jumpingOnClouds(int[] clouds) {
        int i = 0;
        int jumps = 0;

        while (i < clouds.length - 1) {
            int further = i + 2;
            if (further < clouds.length && clouds[further] != 1) {
                i += 2;
            } else {
                i++;
            }

            jumps++;
        }

        return jumps;
    }

    @Test
    public void test1() {
        final int result = jumpingOnClouds(new int[] {0, 0, 0, 0, 1, 0});
        assertThat(result).isEqualTo(3);

        final int result2 = jumpingOnClouds(new int[] {0, 0, 1, 0, 0, 1, 0});
        assertThat(result2).isEqualTo(4);

        final int result3 = jumpingOnClouds(new int[] {0, 0, 0, 1, 0, 0});
        assertThat(result3).isEqualTo(3);
    }

}
