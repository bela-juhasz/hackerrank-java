package minimal;

import org.junit.Test;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import java.util.Scanner;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class ShortestReachInAGraphTest {

    static int[] collectDistances(int numberOfNodes, int[][] edges, int startingNodeId) {
        final SortedMap<Integer, Node> sortedNodes = buildGraph(numberOfNodes, edges);

        final Node startingNode = sortedNodes.remove(startingNodeId);

        final Set<Node> nodesWithDistances = breadthFirstSearchReturningDistances(startingNode);

        final int[] sortedDistances = new int[numberOfNodes - 1];
        int index = 0;

        for (final Node node : sortedNodes.values()) {

            final int distance;

            if (nodesWithDistances.contains(node)) {
                distance = node.level * 6;
            } else {
                distance = -1;
            }

            sortedDistances[index++] = distance;
        }

        return sortedDistances;
    }

    private static Set<Node> breadthFirstSearchReturningDistances(final Node startingNode) {
        final Deque<Node> openQueue = new ArrayDeque<>();
        final Set<Node> closedSet = new HashSet<>();

        startingNode.level = 0;
        openQueue.addLast(startingNode);

        while (openQueue.size() != 0) {

            final Node subTreeRoot = openQueue.removeFirst();

            for (final Node relative : subTreeRoot.relatives) {

                if (closedSet.contains(relative)) {
                    continue;
                }

                if (!openQueue.contains(relative)) {
                    relative.level = subTreeRoot.level + 1;
                    openQueue.addLast(relative);
                }
            }

            closedSet.add(subTreeRoot);
        }

        return closedSet;
    }

    private static SortedMap<Integer, Node> buildGraph(final int numberOfNodes, final int[][] edges) {
        final SortedMap<Integer, Node> nodes = new TreeMap<>();

        for (final int[] edge : edges) {
            int fromNodeId = edge[0];
            int toNodeId = edge[1];

            final Node fromNode = getOrCreateNodeById(fromNodeId, nodes);
            final Node toNode = getOrCreateNodeById(toNodeId, nodes);

            fromNode.relatives.add(toNode);
            toNode.relatives.add(fromNode);
        }

        // also create unconnected nodes which were not specified by the edges matrix
        for (int nodeId = 1; nodeId <= numberOfNodes; nodeId++) {
            getOrCreateNodeById(nodeId, nodes);
        }

        return nodes;
    }

    private static Node getOrCreateNodeById(final int id, final Map<Integer, Node> nodes) {
        if (nodes.containsKey(id)) {
            return nodes.get(id);
        } else {
            final Node newNode = new Node(id);
            nodes.put(id, newNode);
            return newNode;
        }
    }

    public static class Node {
        public int id;
        public long color;
        public List<Node> relatives = new ArrayList<>();
        public boolean discovered;
        public int level;

        public Node(int id) {
            this.id = id;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            final Node node = (Node) o;
            return id == node.id;
        }

        @Override
        public int hashCode() {
            return Objects.hash(id);
        }

        @Override
        public String toString() {
            return "Node{" +
                "id=" + id +
                ", color=" + color +
                (discovered ? ", discovered" : ", not discovered") +
                ", level=" + level +
                ", relatives=" + relatives.stream().map(n -> n.id).collect(Collectors.toList()) +
                '}';
        }
    }

    public static class Graph {

        private int size;
        private int[][] adjMatrix;

        public Graph(final int size) {
            this.size = size;
            adjMatrix = new int[size][size];
        }

        public void addEdge(int from, int to) {
            adjMatrix[from][to] = 1;
            adjMatrix[to][from] = 1;

            LinkedList[] adj = new LinkedList[3];
            adj[0] = new LinkedList();
        }

        public int[] calculateDistances(int startId) {
            final int[] distances = new int[size];
            Arrays.fill(distances, -1);

            final Queue<Integer> queue = new LinkedList<>();
            queue.add(startId);

            while (!queue.isEmpty()) {

                int from = queue.remove();

                for (int to = 0; to < size; to++) {

                    if (to == from) {
                        distances[from] = 0;
                    }

                    if (
                        adjMatrix[from][to] == 1 && // if edgeExists and
                        distances[to] == -1 // notVisitedYet
                    ) {
                        queue.add(to);
                        distances[to] = distances[from] + 6;
                    }
                }
            }

            return distances;
        }
    }

    public static void main(String[] args) {
        //Scanner scan = new Scanner(System.in);
        Scanner scan = new Scanner(String.join("", args));
        int numQueries = scan.nextInt();

        for (int q = 0; q < numQueries; q++) {
            int numNodes = scan.nextInt();
            int numEdges = scan.nextInt();

            final Graph graph = new Graph(numNodes);

            for (int i = 0; i < numEdges; i++) {
                int from = scan.nextInt();
                int to = scan.nextInt();
                graph.addEdge(from - 1, to - 1);
            }

            int startId = scan.nextInt();
            final int[] distances = graph.calculateDistances(startId - 1);

            for (int i = 1; i <= numNodes; i++) {
                if (i != startId) {
                    System.out.print(distances[i] + " ");
                }
            }

            System.out.println();
        }

        scan.close();
    }

    @Test
    public void test1() {

        final String[] sampleInput = new String[] {
            "2\n" +
                "4 2\n" +
                "1 2\n" +
                "1 3\n" +
                "1\n" +
                "3 1\n" +
                "2 3\n" +
                "2"
        };

        main(sampleInput);

        assertThat(collectDistances(4,
            new int[][]{
                {1, 2},
                {1, 3}
            }, 1)).containsSequence(6, 6, -1);


        assertThat(collectDistances(3,
            new int[][]{
                {2, 3}
            }, 2)).containsSequence(-1, 6);
    }

}

