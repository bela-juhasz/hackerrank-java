package minimal;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TemplateTest {

    static int countingValleys(final int numberOfSteps, final String steps) {
        int level = 0;
        boolean weCameFromAValley = false;
        boolean weJustStarted = true;
        int numberOfValleys = 0;

        for (int i=0; i < numberOfSteps; i++) {
            final char step = steps.charAt(i);

            switch (step) {
                case 'U':
                    level++;
                    break;
                case 'D':
                    level--;
                    break;
                default:
            }

            if (level < 0) {
                weCameFromAValley = true;
            }

            if (level == 0 && weJustStarted == false && weCameFromAValley) {
                numberOfValleys++;
                weCameFromAValley = false;
            }

            weJustStarted = false;
        }

        return numberOfValleys;
    }

    @Test
    public void test1() {
        /*
        long[] array = new long[n];

        new int[] { 1, 2, 3, 4, 5 };

        new int[][] {
            { 1, 2, 1 },
            { 2, 5, 1 },
            { 3, 4, 1 }
        };
        Arrays.asList("1100", "1110", "0110", "0001")

        for (int i = 0; i < s.length; i++) {
        }



        for (int row = 0; row <= rows - 3; row++) {
            for (int col = 0; col <= cols - 3; col++) {
            }
        }
        */


        assertThat(countingValleys(8, "UDDDUDUU")).isEqualTo(1);
    }

}
