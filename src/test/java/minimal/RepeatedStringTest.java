package minimal;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class RepeatedStringTest {

    // https://github.com/gabrielgiordan/HackerRank

    static long repeatedString(String s, long n) {
        long numOfS = n / s.length();
        long rest = n % s.length();

        if(!s.contains("a")) {
            return 0;
        }

        if (s.length() > n) {
            return count(s, rest);
        } else {
            return numOfS * count(s, s.length()) + count(s, rest);
        }
    }

    private static long count(String s, long endIndex) {
        int sum = 0;
        for (int i=0; i < endIndex; i++) {
            if (s.charAt(i) == 'a') {
                sum++;
            }
        }
        return sum;
    }

    @Test
    public void test1() {
        assertThat(repeatedString("aba", 10)).isEqualTo(7);
        assertThat(repeatedString("a", 1000000000000L)).isEqualTo(1000000000000L);
    }

}
