package minimal;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class HourGlassSumTest {

    // https://github.com/gabrielgiordan/HackerRank



    static int hourglassSum(int[][] arr) {
        int rows = arr.length;
        int cols = arr[0].length;
        int max = Integer.MIN_VALUE;

        for (int row = 0; row <= rows - 3; row++) {
            for (int col = 0; col <= cols - 3; col++) {
                int sum = arr[row][col] + arr[row][col+1] + arr[row][col+2] +
                    arr[row+1][col+1] +
                    arr[row+2][col] + arr[row+2][col+1] + arr[row+2][col+2];
                if (sum > max) {
                    max = sum;
                }
            }
        }

        return max;
    }

    @Test
    public void test1() {
        final int[][] multi = new int[][] {
            { 1, 1, 1, 0, 0, 0 },
            { 0, 1, 0, 0, 0, 0 },
            { 1, 1, 1, 0, 0, 0 },
            { 0, 0, 2, 4, 4, 0 },
            { 0, 0, 0, 2, 0, 0 },
            { 0, 0, 1, 2, 4, 0 }
        };

        assertThat(hourglassSum(multi)).isEqualTo(19);
    }

}
