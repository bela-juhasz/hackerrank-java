package minimal;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ArrayRotationLeftTest {

    static int[] rotLeft(int[] arr, int d) {
        int[] newArray = new int[arr.length];
        int j = 0;

        for (int i = d; i < arr.length; i++) {
            newArray[j++] = arr[i];
        }

        for (int i = 0; i < d; i++) {
            newArray[j++] = arr[i];
        }

        return newArray;
    }

    @Test
    public void test1() {
        assertThat(rotLeft(new int[] { 1, 2, 3, 4, 5 }, 4)).containsSequence(5, 1, 2, 3, 4);
    }

}
