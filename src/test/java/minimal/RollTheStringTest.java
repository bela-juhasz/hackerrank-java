package minimal;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

public class RollTheStringTest {

    public static String rollTheSt2ring(String s, List<Integer> rollList) {
        int roll[] = new int[rollList.size()];

        for (int i = 0; i < rollList.size(); i++) {
            roll[i] = rollList.get(0);
        }

        char[] charr = s.toCharArray();
        for (int i = 0; i < rollList.size(); i++) {
            for (int j = 0; j < roll[i]; j++) {
                if (charr[j] != 'z')
                    charr[j] = (char) ((int) charr[j] + 1);
                else
                    charr[j] = 'a';
            }
        }

        return String.valueOf(charr);

    }

    public static String rollTheString3(String str, List<Integer> rollList) {
        int roll[] = new int[rollList.size()];

        for (int i = 0; i < rollList.size(); i++) {
            roll[i] = rollList.get(0);
        }

        int n = roll.length;
        int[] count = new int[n + 1];

        for (int i = 0; i < n; i++)
            count[roll[i]]++;

        for (int i = n - 1; i > 0; i--) {
            count[i] += count[i + 1];
        }

        StringBuffer sb = new StringBuffer();
        for (int i = 1; i <= n; i++) {
            char ch = str.charAt(i - 1);
            int temp = ((ch - 'a') + (count[i] % 26)) % 26;
            sb.append((char) (temp + 'a'));
        }

        return sb.toString();
    }

    public static String rollTheString2(String toRoll, List<Integer> rollList) {
        int roll[] = new int[rollList.size()];

        for (int i = 0; i < rollList.size(); i++) {
            roll[i] = rollList.get(0);
        }

        int toRollLength = toRoll.length();
        int rollCounts[] = new int[toRollLength];

        Arrays.stream(roll).forEach(rollCount -> rollCounts[rollCount - 1]++);

        for (int index = toRollLength - 2; index >= 0; index--) {
            rollCounts[index] += rollCounts[index + 1];
        }

        char toRollArray[] = toRoll.toCharArray();
        IntStream.range(0, toRollLength).forEach(index -> {
            toRollArray[index] = (char) ('a' + (rollCounts[index] % 26 + toRollArray[index] - 'a') % 26);
        });

        return String.valueOf(toRollArray);
    }


    public static String rollTheString22(String s, List<Integer> roll) {

        int len = s.length();
        int[] charCodes = new int[len];

        char[] charArray = s.toCharArray();

        for (int i = 0; i < len; i++) {
            charCodes[i] = (int) charArray[i] - 97;
        }

        for (Integer aRoll : roll) {
            for (int j = 0; j < aRoll; j++) {
                charCodes[j]++;

                if (charCodes[j] > 25) {
                    charCodes[j] = charCodes[j] - 26;
                }
            }
        }

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < len; i++) {
            char c = (char) (charCodes[i] + 97);
            sb.append(c);
        }

        return sb.toString();
    }

    public static String rollTheString(String s, List<Integer> roll) {

        int len = s.length();
        int[] charCodes = new int[len];

        char[] charArray = s.toCharArray();

        for (int i = 0; i < len; i++) {
            charCodes[i] = (int) charArray[i] - 97;
        }

        int[] rollPositionSum = new int[len];

        for (int i = 0; i < roll.size(); i++) {
            final Integer rol = roll.get(i);
            rollPositionSum[rol - 1] = mod26(rollPositionSum[rol - 1] + 1);
        }

        int rollSum = 0;

        for (int i = len-1; i >= 0; i--) {
            rollSum = mod26(rollSum + rollPositionSum[i]);

            charCodes[i] = mod26(charCodes[i] + rollSum);
        }

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < len; i++) {
            char c = (char) (charCodes[i] + 97);
            sb.append(c);
        }

        return sb.toString();
    }
    
    private static int mod26(int c) {
        return (c > 25) ?  c - 26 : c; 
    }


    @Test
    public void test1() {
        assertThat(rollTheString("abz", Arrays.asList(3))).isEqualTo("bca");
        assertThat(rollTheString("abz", Arrays.asList(3))).isEqualTo("bca");
        assertThat(rollTheString("abz", Arrays.asList(3, 2))).isEqualTo("cda");
        assertThat(rollTheString("abz", Arrays.asList(3, 2, 1))).isEqualTo("dda");
        //assertThat(rollTheString("abz", Arrays.asList(0))).isEqualTo("dda");

        assertThat(rollTheString("vwxyz", Arrays.asList(1, 2, 3, 4, 5))).isEqualTo("aaaaa");



        assertThat('a' - 97).isEqualTo(0);
        assertThat('z' - 97).isEqualTo(25);
    }

}
